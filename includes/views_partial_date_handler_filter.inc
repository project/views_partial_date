<?php

class views_partial_date_handler_filter extends views_handler_filter_numeric {

  // Default to month
  const DEFAULT_FORMAT = 'm';

  /** @var date_sql_handler $date_handler **/
  var $date_handler = NULL;

  /** @var views_plugin_query_default $query **/
  var $query = NULL;

  function init(&$view, &$options) {
    parent::init($view, $options);
    $field = field_info_field($this->definition['field_name']);
    module_load_include('inc', 'date_api', 'date_api_sql');
    $this->date_handler = new date_sql_handler($field['type']);
    $this->date_handler->db_timezone = date_get_timezone_db($field['settings']['tz_handling']);
    $this->date_handler->local_timezone = date_get_timezone($field['settings']['tz_handling']);
    $this->date_handler->granularity = 'minute';
    $this->base_table = $this->table;
  }

  function operators() {
    $supported_operators = array('<', '<=', '=', '!=', '>=', '>', 'between', 'not between');
    return array_intersect_key(parent::operators(), array_flip($supported_operators));
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['format'] = array('default' => self::DEFAULT_FORMAT);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $format = $this->options['format'];
    $format_label = $this->get_format_label($format);

    $element = &$form['value'];
    $base = array(
      '#type' => 'select',
      '#size' => 1,
      '#options' => $this->get_format_ranges($format),
    );
    // @todo Poor man's translation. Needs a little more love.
    $element['value'] = array(
      '#title' => ucfirst($format_label),
    ) + $base + $element['value'];
    $element['min'] = array(
        '#title' => $element['min']['#title'] . ' ' . $format_label
      ) + $base + $element['min'];
    $element['max'] = array(
        '#title' => $element['max']['#title'] . ' ' . $format_label
      ) + $base + $element['max'];
  }

  function has_extra_options() {
    return true;
  }

  function extra_options_form(&$form, &$form_state) {
    $form['format'] = array(
      '#type' => 'select',
      '#title' => t('Granularity'),
      '#options' => $this->get_format_options(),
      '#default_value' => $this->options['format'],
    );
  }

  function extra_options_submit($form, &$form_state) {
    $format = $form_state['values']['options']['format'];
    // Reset values if the format was changed
    if($format !== $this->options['format']) {
      $form_state['values']['options']['value'] = array(
        'value' => null,
        'min' => null,
        'max' => null,
      );
    }
    parent::extra_options_submit($form, $form_state);
  }

  function op_between($field) {
    $format = $this->options['format'];

    $field_min = $this->date_handler->sql_field($field);
    $field_min = $this->date_handler->sql_format($format, $field_min);
    $field_max = $this->date_handler->sql_field($field);
    $field_max = $this->date_handler->sql_format($format, $field_max);
    $placeholder_min = $this->placeholder();
    $placeholder_max = $this->placeholder();
    $group = $this->options['group'];
    if ($this->operator == 'between') {
      $this->query->add_where_expression($group, "$field_min >= $placeholder_min AND $field_max <= $placeholder_max", array(
        $placeholder_min => $this->value['min'],
        $placeholder_max => $this->value['max']
      ));
    }
    else {
      $this->query->add_where_expression($group, "$field_min < $placeholder_min OR $field_max > $placeholder_max", array($placeholder_min => $min_value, $placeholder_max => $max_value));
    }
  }

  function op_simple($field) {
    $format = $this->options['format'];
    $field = $this->date_handler->sql_field($field, NULL);
    $field = $this->date_handler->sql_format($format, $field);
    $placeholder = $this->placeholder();
    $this->query->add_where_expression($this->options['group'], "$field $this->operator $placeholder", array(
      $placeholder => $this->value['value']
    ));
  }

  function get_format_options() {
    return array(
      'm' => t('month'),
      'W' => t('week'),
      'd' => t('day'),
      'D' => t('day of the week'),
      'H' => t('hour'),
      'i' => t('minute'),
    );
  }

  function get_format_label($format) {
    $labels = $this->get_format_options();
    return isset($labels[$format]) ? $labels[$format] : t('Invalid format');
  }
  
  function get_format_ranges($format) {
    switch($format) {
      // Month
      case 'm': return $this->pad_range(date_month_names(true), 2);
      // Week of the year
      case 'W': return array_combine(range(1, 53), range(1, 53));
      // Day of the month
      case 'd': return $this->pad_range(range(1, 31));
      // Day of the week
      case 'D': return array(
        'Mon' => t('Monday'),
        'Tue' => t('Tuesday'),
        'Wed' => t('Wednesday'),
        'Thu' => t('Thursday'),
        'Fri' => t('Friday'),
        'Sat' => t('Saturday'),
        'Sun' => t('Sunday'),
      );
      // Hour
      case 'H': return $this->pad_range(range(0, 23));
      // Minute
      case 'i': return $this->pad_range(range(0, 59), 2, true);

      default: return array();
    }
  }

  /**
   * Helper function to zero-pad numeric option keys and values.
   */
  function pad_range($range, $size = 2, $pad_value = false) {
    $values = array();
    foreach($range as $key => $value) {
      $key = sprintf("%0{$size}.{$size}u", $key);
      if($pad_value) {
        $value = sprintf("%0{$size}.{$size}u", $value);
      }
      $values[$key] = $value;
    }
    return $values;
  }

  function admin_summary() {
    $format = $this->options['format'];
    $format_label = $this->get_format_label($format);
    $values = isset($this->options['value']) ? $this->options['value'] : array() + array(
        'min' => null,
        'max' => null,
        'value' => null
      );

    if ($this->options['exposed']) {
      return t('<strong>Exposed</strong> @format', array('@format' => $format_label));
    }

    $format_options = $this->get_format_ranges($format);
    $operator_options = $this->operator_options('short');
    $output = $format_label . ' ' . check_plain($operator_options[$this->operator]) . ' ';

    if (in_array($this->operator, $this->operator_values(2))) {
      $min = $format_options[ $values['min'] ];
      $max = $format_options[ $values['max'] ];
      $output .= t('@min and @max', array('@min' => $min, '@max' => $max));
    }
    else {
      $output .= check_plain($format_options[ $values['value'] ]);
    }
    return $output;
  }

}
